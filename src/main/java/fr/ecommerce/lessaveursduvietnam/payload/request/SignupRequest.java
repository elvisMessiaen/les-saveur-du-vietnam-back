package fr.ecommerce.lessaveursduvietnam.payload.request;

import java.util.Set;

import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import fr.ecommerce.lessaveursduvietnambeans.dao.Role;


public class SignupRequest {
	@NotBlank
    @Size(min = 3, max = 20)
    private String username;
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
    
    private Set<Role> roles;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
    
    
  
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Set<Role> getRoles() {
      return this.roles;
    }
    
    public void setRoles(Set<Role> role) {
      this.roles = role;
    }

    public SignupRequest() {
    	
    }
	public SignupRequest(@NotBlank @Size(min = 3, max = 20) String username,
			@NotBlank @Size(max = 50) @Email String email, @NotBlank @Size(min = 6, max = 40) String password) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
	}
	
}
