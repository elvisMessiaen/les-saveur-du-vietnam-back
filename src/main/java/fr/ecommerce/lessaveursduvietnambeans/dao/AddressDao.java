//package fr.ecommerce.lessaveursduvietnambeans.dao;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.Size;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//@Data
//@AllArgsConstructor 
//@NoArgsConstructor 
//@Entity 
//	public class AddressDao {
//		@Id 
//		@GeneratedValue(strategy = GenerationType.SEQUENCE)  
//		private Long idAdresse;
//		
//		@NotBlank
//		@Size(max = 5)
//		@Column(name="numVoie",nullable = false) 
//		private int numVoie;
//		
//		@NotBlank
//		@Column(name="nomVoie",nullable = false) 
//		private String nomVoie;
//		
//		@NotBlank
//		@Size(max = 5)
//		@Column(name="codePostal",nullable = false)
//		private int codePostal;
//		
//		
//		@Column(name="ville",nullable = false)
//		private String ville;
//		
//		
//		@ManyToOne()
//		@JoinColumn(name= "idUser")
//		private UserDao user;
//
//
//		public AddressDao(@NotBlank @Size(max = 5) int numVoie, @NotBlank String nomVoie,
//				@NotBlank @Size(max = 5) int codePostal, String ville) {
//			super();
//			this.numVoie = numVoie;
//			this.nomVoie = nomVoie;
//			this.codePostal = codePostal;
//			this.ville = ville;
//
//		}
//		
//		
//
//	}
//
//
